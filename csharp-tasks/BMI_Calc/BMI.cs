namespace csharp_tasks.BMI_Calc
{
    static class BMI
    {
        public static double CalculateBMI(double weight, double height) => weight / (height * height);

        public static string Judge(double bmi)
        {
            if(bmi < 18.5)
            {
                return "Underweight";
            }
            else if(bmi <= 24.9)
            {
                return "Normal weight";
            }
            else if(bmi <= 29.9)
            {
                return "Overweight";
            }
            else
            {
                return "Obese";
            }
        }
    }
}