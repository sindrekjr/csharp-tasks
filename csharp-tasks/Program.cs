﻿using System;
using System.Text;
using System.Threading;

using csharp_tasks.BMI_Calc;

namespace csharp_tasks
{
    class Program
    {
        static string username;

        static void Main(string[] args)
        {
            // Task: HelloYou
            ProcessGreeting();

            // Task: PrintSquare
            Console.WriteLine("\nNow let's do something vaguely interactive..");
            Thread.Sleep(1000);
            MakeSquareFunGameYay();

            // Task: YellowPages
            LookForFolks();

            // Task: BMI
            CheckBMI();

            // Task: Fibonacci
            Lateralus();
        }

        static void ProcessGreeting()
        {
            Console.WriteLine("Hello! Gimme your name.");
            username = Console.ReadLine();
            Console.WriteLine($"Looks like your name starts with a \"{username[0]}\", is {username.Length} characters long, and has bytecode {Convert.ToBase64String(Encoding.ASCII.GetBytes(username))}. NICE.");
        }

        static void MakeSquareFunGameYay()
        {
            int height, width;
            try
            {
                Console.WriteLine("\nEnter height and width of a square, separated by spaces. We'll make it for you!");
                string[] input = Console.ReadLine().Split(' ');
                height = int.Parse(input[0]);
                width = int.Parse(input[1]);

                Console.WriteLine($"Rendering height {input[0]} and width {input[1]}. Hold on...");
            }
            catch(Exception)
            {
                Console.WriteLine("Something went wrong. We'll just default to 10:10, whatever.");
                height = 10;
                width = 10;
            }

            Thread.Sleep(2000);
            new Square(height, width).Draw();
        }

        static void LookForFolks()
        {
            var pages = new YellowPages();

            Console.WriteLine("\nWho are you looking for? If you want all matches, end your input with the command argument \"-a\" (without quotes).");
            string input = Console.ReadLine();

            try
            {
                if(input.Contains("-a"))
                {
                    pages.GetAll(input.Replace("-a", "")).ForEach(p => Console.WriteLine(p.Name));
                }
                else
                {
                    Console.WriteLine(pages.Get(input).Name);
                }
            }
            catch(NullReferenceException)
            {
                Console.WriteLine("Whoops. Looks like there's no one matching your search.");
            }
            catch(InvalidOperationException)
            {
                Console.WriteLine("Whoops. Looks like there's no one matching your search.");
            }

        }

        private static void CheckBMI()
        {
            Console.WriteLine("\nIf you want, we can also calculate BMI. All you gotta do is input your weight (kg) and height (cm) separated by space. BMI is a horrible metric though.");
            string[] input = Console.ReadLine().Split(' ');

            try
            {
                double weight = int.Parse(input[0]);
                double height = int.Parse(input[1]);
                double bmi = BMI.CalculateBMI(weight, height / 100);

                Console.WriteLine($"Alright! Your BMI looks to be: {bmi} ({BMI.Judge(bmi)})");
            }
            catch(Exception)
            {
                Console.WriteLine("Something went wrong. We'll just move on.");
            }
        }

        static void Lateralus()
        {
            int previous = 1;
            int current = 1;
            int sum = 0;
            while(current < 4000000)
            {
                int n = current + previous;
                previous = current;
                current = n;

                if(current % 2 == 0) sum += current;
            }

            Console.WriteLine($"\nAlso, here's the sum of all even numbers in the fibonacci sequence that are below four million: {sum}");
        }
    }
}
