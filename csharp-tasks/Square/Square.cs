﻿using System;

namespace csharp_tasks
{
    class Square
    {
        string square;

        public Square(int height = 10, int width = 10) => MakeSquare(height, width);

        public string GetSquare() => this.square;

        public void Draw() => Console.Write(this.square);

        private void MakeSquare(int height, int width)
        {
            this.square = "\n";
            for (int i = 0; i < height; i++)
            {
                string output = "";
                for (int j = 0; j < width; j++)
                {
                    output += (i == 0 || i == height - 1 || j == 0 || j == width - 1) ? '#' : ' ';
                }

                this.square += $"{output}\n";
            }
        }
    }
}
