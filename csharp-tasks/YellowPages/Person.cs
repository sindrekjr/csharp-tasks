﻿using System.Linq;

namespace csharp_tasks
{
    class Person
    {
        string firstname;
        string lastname;

        public string Name { get => $"{firstname} {lastname}"; }
        public int Phone { get; set; }

        public Person(string firstname, string lastname)
        {
            this.firstname = firstname;
            this.lastname = lastname;
        }

        public Person(string firstname, string lastname, int phone) : this(firstname, lastname)
        {
            this.Phone = phone;
        }

        public Person(string name) => parseName(name);

        private void parseName(string name)
        {
            string[] names = name.Split(' ');
            firstname = string.Join(' ', names.Take(names.Length - 1));
            lastname = names.Last();
        }
    }
}
