﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_tasks
{
    class YellowPages
    {

        public List<Person> People;

        public YellowPages() => GenerateRandomPeople();

        internal Person Get(string searchString) => GetAll(searchString).First();

        internal List<Person> GetAll(string searchString) => People.Where(p => p.Name.ToLower().Contains(searchString.ToLower().Trim())).ToList();

        private void GenerateRandomPeople()
        {
            string[] firstnames = new string[] {"Jack", "Jill", "James", "Jackson", "Robert", "Regina", "Rigena", "Rickard" };
            string[] names = new string[] {"Jackson", "Ampersand", "Palmer", "Jameson", "Koch", "Pedersen", "Touche", "Malarkey" };

            People = new List<Person>();
            var rnd = new Random();

            for(int i = 0; i < 5; i++)
            {
                string first = firstnames[rnd.Next(firstnames.Length)];
                string last = "";
                for (int j = 0; j < rnd.Next(1, 3); j++) last += $"{names[rnd.Next(names.Length)]} ";
                People.Add(new Person(first, last.Trim()));
            }
        }
    }
}
